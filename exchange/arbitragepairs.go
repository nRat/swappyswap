package exchange

import (
	"fmt"
	"sync"
	"time"

	"codeberg.org/nrat/swappyswap/exchange"
)

func FindArbitragePairs(uniswap exchange.Exchange, sushiswap exchange.Exchange, pairs []CurrencyPair, opportunityCh chan<- ArbitrageOpportunity, wg *sync.WaitGroup) {
	defer wg.Done()

	for _, pair := range pairs {
		// Get token prices from Uniswap and SushiSwap
		uniswapPrice := uniswap.GetTokenPrice(pair.Token1)
		sushiswapPrice := sushiswap.GetTokenPrice(pair.Token2)

		// Calculate price difference and check for arbitrage opportunity
		priceDiff := sushiswapPrice - uniswapPrice
		if priceDiff > 0 {
			opportunity := ArbitrageOpportunity{
				Exchange:    "SushiSwap",
				Token:       fmt.Sprintf("%s-%s", pair.Token1, pair.Token2),
				PriceSource: "SushiSwap",
				PriceDiff:   priceDiff,
				OpenTime:    time.Now(),
			}
			opportunityCh <- opportunity
		} else if priceDiff < 0 {
			opportunity := ArbitrageOpportunity{
				Exchange:    "Uniswap",
				Token:       fmt.Sprintf("%s-%s", pair.Token1, pair.Token2),
				PriceSource: "Uniswap",
				PriceDiff:   -priceDiff,
				OpenTime:    time.Now(),
			}
			opportunityCh <- opportunity
		}
	}
}
