package exchange

import (
	"fmt"
	"time"
)

func LogArbitrageOpportunities(opportunityCh <-chan ArbitrageOpportunity) {
	for opportunity := range opportunityCh {
		fmt.Printf("Arbitrage Opportunity: Exchange=%s, Token=%s, PriceDiff=%f, PriceSource=%s, OpenTime=%s\n",
			opportunity.Exchange, opportunity.Token, opportunity.PriceDiff, opportunity.PriceSource, opportunity.OpenTime)

		// Simulate an opportunity window closing after 5 seconds
		time.Sleep(5 * time.Second)
		fmt.Printf("Opportunity Window Closed: Exchange=%s, Token=%s\n",
			opportunity.Exchange, opportunity.Token)
	}
}
