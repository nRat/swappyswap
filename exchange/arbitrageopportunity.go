package exchange

import "time"

type ArbitrageOpportunity struct {
	Exchange    string
	Token       string
	PriceSource string
	PriceDiff   float64
	OpenTime    time.Time
}
