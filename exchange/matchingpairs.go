package exchange

import (
	"log"

	"codeberg.org/nrat/swappyswap/exchange"
)

func FindMatchingPairs(uniswap exchange.Exchange, sushiswap exchange.Exchange) []CurrencyPair {
	// Get all available tokens from Uniswap
	uniswapTokens, err := uniswap.GetAvailableTokens()
	if err != nil {
		log.Fatal(err)
	}

	// Get all available tokens from SushiSwap
	sushiswapTokens, err := sushiswap.GetAvailableTokens()
	if err != nil {
		log.Fatal(err)
	}

	var matchingPairs []CurrencyPair

	// Find matching pairs available on both exchanges
	for _, uniswapToken := range uniswapTokens {
		for _, sushiswapToken := range sushiswapTokens {
			if uniswapToken == sushiswapToken {
				pair := CurrencyPair{
					Token1: uniswapToken,
					Token2: sushiswapToken,
				}
				matchingPairs = append(matchingPairs, pair)
			}
		}
	}

	return matchingPairs
}
