package exchange

type Exchange interface {
	GetAvailableTokens() ([]string, error)
	GetTokenPrice(tokenAddress string) float64
}
