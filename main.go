package main

import (
	"log"
	"sync"

	"codeberg.org/nrat/swappyswap/exchange"
	"codeberg.org/nrat/swappyswap/exchange/sushiswap"
	"codeberg.org/nrat/swappyswap/exchange/uniswap"
	"github.com/ethereum/go-ethereum/rpc"
)

type CurrencyPair struct {
	Token1 string
	Token2 string
}

func main() {
	// Connect to the Ethereum network
	client, err := rpc.Dial("https://mainnet.infura.io/v3/your_infura_project_id")
	if err != nil {
		log.Fatal(err)
	}

	// Create instances of Uniswap and SushiSwap exchanges
	uniswap := uniswap.NewUniswap(client)
	sushiswap := sushiswap.NewSushiSwap(client)

	// Find matching pairs available on both exchanges
	pairs := exchange.FindMatchingPairs(uniswap, sushiswap)

	// Create a channel to receive arbitrage opportunities
	opportunityCh := make(chan exchange.ArbitrageOpportunity)

	// Create a wait group to synchronize goroutines
	var wg sync.WaitGroup

	// Start finding arbitrage pairs
	wg.Add(1)
	go exchange.FindArbitragePairs(uniswap, sushiswap, pairs, opportunityCh, &wg)

	// Start logging the arbitrage opportunities
	go exchange.LogArbitrageOpportunities(opportunityCh)

	// Wait for all goroutines to finish
	wg.Wait()

	// Handle any cleanup or shutdown tasks
}
